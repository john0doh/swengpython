# import the unittest library
import unittest

# Class to represent node in tree


class Node:

    # Constructor for class
    def __init__(self, key):

        self.rightNode = None
        self.leftNode = None
        self.key = key


def findPath(root, key, path):

    if root is None:
        return False

    path.append(root.key)

    if root.key == key:
        return True

    if root.leftNode != None and findPath(root.leftNode, key, path):
        return True

    if root.rightNode != None and findPath(root.rightNode, key, path):
        return True

    # else
    path.pop()
    return False


def findLCA(root, key1, key2):

    path1 = []
    path2 = []

    if (not findPath(root, key1, path1)) or (not findPath(root, key2, path2)):
        return -1

    i = 0
    while (i < len(path1) and i < len(path2)):
        if(path1[i] != path2[i]):
            break
        i += 1

    return path1[i - 1]


class TestLCAPython3(unittest.TestCase):
    """
    Test class for implementing unit testing on the LCA algorithm
    """

    def test_constructor(self):
        node = Node(0)
        self.assertEquals(node.key, 0)
        self.assertEquals(node.leftNode, None)
        self.assertEquals(node.rightNode, None)

    def test_findPath(self):
        root = None
        # Test that if node is None then we return false
        self.assertFalse(findPath(root, 1, []))

        root = Node(1)
        # test that if it finds the path then it returns true
        self.assertTrue(findPath(root, 1, []))

        root = self.mkTestTree(root)
        path = []
        # Test that it finds a path if the given key exists in the tree
        self.assertTrue(findPath(root, 4, path))
        path = []  # path needs to be reset
        self.assertTrue(findPath(root, 6, path))

        path = []
        #Test that it fails when passed key that isn't in tree
        self.assertFalse(findPath(root,10,path))

        #Testing done :)

    def test_findLCA(self):
        """
        Tests the findLCA method for correct behaviour
        """
        root = None
        #test if bad value passed that it returns -1
        self.assertEquals(findLCA(root,1,2),-1)

        root = Node(1)
        #test if searching for same key works
        self.assertEquals(findLCA(root,1,1),root.key)

        root = self.mkTestTree(root)
        #Establish tree and test full functionality
        self.assertEquals(findLCA(root,3,4),2)
        self.assertEquals(findLCA(root,7,6),5)
        self.assertEquals(findLCA(root,2,5),1)
        self.assertEquals(findLCA(root,3,7),1)

    def mkTestTree(self, root):
        """
        Makes a standard tree that is used for testing purposes
        How is this tested?
        """
        root = Node(1)
        root.rightNode = Node(2)
        root.rightNode.rightNode = Node(3)
        root.rightNode.leftNode = Node(4)
        root.leftNode = Node(5)
        root.leftNode.rightNode = Node(6)
        root.leftNode.leftNode = Node(7)
        return root


if __name__ == '__main__':
    unittest.main()
